//
//  NetworkManager.swift
//  Teste IOS
//
//  Created by Murilo de Souza Lopes on 02/05/2019.
//  Copyright © 2019 Murilo de Souza Lopes. All rights reserved.
//

import Foundation
import Alamofire

class NetworkManager{
    
    typealias APIResultCall = (Data?) -> ()
    
    static let shared = NetworkManager()
    
    private let baseURL = "https://pastebin.com/raw/qdfq10ii"
    
    private init(){
        
    }
    
    func getProducts(url:String, completion: @escaping ([Product]) -> Void){
        
        let arrayProducts = [Product]()
        
        Alamofire.request(url, parameters: nil).responseData { (result) in
            if let dataReturn = result.value{
                let decoder = JSONDecoder()
                do{
                    let decodeProducts = try decoder.decode(ProductRoot.self, from: dataReturn)
                    completion(decodeProducts.products)
                }catch{
                    completion(arrayProducts)
                }
            }else{
                completion(arrayProducts)
            }
        }
    }

}
