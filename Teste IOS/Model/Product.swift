//
//  Product.swift
//  Teste IOS
//
//  Created by Murilo de Souza Lopes on 02/05/2019.
//  Copyright © 2019 Murilo de Souza Lopes. All rights reserved.
//

import Foundation

struct Product: Codable{
    var title: String?
    var price_float: Float
    var default_photo:DefaultPhoto
    var photos: [Photo]
}

struct ProductRoot: Codable {
    var products: [Product]
}

struct Photo: Codable{
    var image_public_id: String
}

struct DefaultPhoto: Codable{
    var image_public_id: String
}

