//
//  ProductPresenter.swift
//  Teste IOS
//
//  Created by Murilo de Souza Lopes on 04/05/2019.
//  Copyright (c) 2019 Murilo de Souza Lopes. All rights reserved.
//


import UIKit

protocol ProductPresentationLogic
{
  func presentSomething(response: ProductDetail.Something.Response)
}

class ProductPresenter: ProductPresentationLogic
{
  weak var viewController: ProductDisplayLogic?
  
  // MARK: Do something
  
  func presentSomething(response: ProductDetail.Something.Response)
  {
    let viewModel = ProductDetail.Something.ViewModel()
    viewController?.displaySomething(viewModel: viewModel)
  }
}
