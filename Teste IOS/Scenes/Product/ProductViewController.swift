//
//  ProductViewController.swift
//  Teste IOS
//
//  Created by Murilo de Souza Lopes on 04/05/2019.
//  Copyright (c) 2019 Murilo de Souza Lopes. All rights reserved.
//

import UIKit

protocol ProductDisplayLogic: class
{
  func displaySomething(viewModel: ProductDetail.Something.ViewModel)
}

class ProductViewController: UIViewController, ProductDisplayLogic
{
    var interactor: ProductBusinessLogic?
    var router: (NSObjectProtocol & ProductRoutingLogic & ProductDataPassing)?

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    @IBOutlet weak var currentValue: UILabel!
    @IBOutlet weak var oldValue: UILabel!
    
    var product:Product?
    
    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    

    // MARK: Object lifecycle

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?)
    {
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }

    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        setup()
    }

    // MARK: Setup

    private func setup()
    {
        let viewController = self
        let interactor = ProductInteractor()
        let presenter = ProductPresenter()
        let router = ProductRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }

    // MARK: Routing

    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
    if let scene = segue.identifier {
        let selector = NSSelectorFromString("routeTo\(scene)WithSegue:")
        if let router = router, router.responds(to: selector) {
            router.perform(selector, with: segue)
        }
    }
    }

    // MARK: View lifecycle

    override func viewDidLoad()
    {
        super.viewDidLoad()
        let price = Int((product?.price_float)!)
        let priceOld = price + 20
        oldValue.text = "R$ \(priceOld)"
        currentValue.text = "R$ \(price)"
        doSomething()
        loadPhotos()
    }

    // MARK: Page Controll and Images
    var frame = CGRect(x:0,y:0, width:0, height:0)
    func loadPhotos(){
        
        if let photos = product?.photos{
            let count = photos.count
            pageControl.numberOfPages = count
            
            for index in 0..<count{
                
                let photo = "https://photos.enjoei.com.br/public/180x180/\(photos[index].image_public_id).jpg"
                
                frame.origin.x = scrollView.frame.size.width * CGFloat(index)
                frame.size = scrollView.frame.size
                
                let imgView = UIImageView(frame: frame)
                imgView.load(url: photo)
                self.scrollView.addSubview(imgView)
            }
            
            scrollView.contentSize = CGSize(width:(scrollView.frame.size.width * CGFloat(count)), height: scrollView.frame.size.height)
            
            scrollView.delegate = self
        }
    }
    // MARK: Do something

    //@IBOutlet weak var nameTextField: UITextField!

    func doSomething()
    {
        let request = ProductDetail.Something.Request()
        interactor?.doSomething(request: request)
    }

    func displaySomething(viewModel: ProductDetail.Something.ViewModel)
    {
    //nameTextField.text = viewModel.name
    }
}

extension ProductViewController: UIScrollViewDelegate{
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = scrollView.contentOffset.x / scrollView.frame.size.width
        pageControl.currentPage = Int(pageNumber)
    }
}
