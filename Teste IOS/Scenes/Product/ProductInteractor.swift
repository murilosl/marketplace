//
//  ProductInteractor.swift
//  Teste IOS
//
//  Created by Murilo de Souza Lopes on 04/05/2019.
//  Copyright (c) 2019 Murilo de Souza Lopes. All rights reserved.
//

import UIKit

protocol ProductBusinessLogic
{
  func doSomething(request: ProductDetail.Something.Request)
}

protocol ProductDataStore
{
  var product: Product! {get set }
}

class ProductInteractor: ProductBusinessLogic, ProductDataStore
{
  var presenter: ProductPresentationLogic?
  var worker: ProductWorker?
  var product: Product!
  
  // MARK: Do something
  
  func doSomething(request: ProductDetail.Something.Request)
  {
    worker = ProductWorker()
    worker?.doSomeWork()
    
    let response = ProductDetail.Something.Response()
    presenter?.presentSomething(response: response)
  }
}
