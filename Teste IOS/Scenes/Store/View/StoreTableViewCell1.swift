//
//  StoreTableViewCell.swift
//  Teste IOS
//
//  Created by Murilo de Souza Lopes on 02/05/2019.
//  Copyright © 2019 Murilo de Souza Lopes. All rights reserved.

import UIKit

class StoreTableViewCell1: UITableViewCell {
    
    @IBOutlet weak var image_product: UIImageView!
    @IBOutlet weak var titleProduct: UILabel!
    @IBOutlet weak var price: UILabel!
    
    static let cellIdentifier = "cellProducts"
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    func populate(product: Product){
        if let title = product.title{
            titleProduct.text = title
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
