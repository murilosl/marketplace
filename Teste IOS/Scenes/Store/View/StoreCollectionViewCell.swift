//
//  StoreCollectionViewCell.swift
//  Teste IOS
//
//  Created by Murilo de Souza Lopes on 03/05/2019.
//  Copyright © 2019 Murilo de Souza Lopes. All rights reserved.
//

import UIKit

class StoreCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var productname: UILabel!
    @IBOutlet weak var imageProduct: UIImageView!
    @IBOutlet weak var price: UILabel!
    
    func populate(product: Product){
        productname.text = product.title!
        let priceC = Int(product.price_float)
        price.text = "R$ \(priceC)"
        let photo = "https://photos.enjoei.com.br/public/180x180/\(product.default_photo.image_public_id).jpg"

        imageProduct.load(url: photo)
    }
    
    func configureUI(){
        self.layer.borderColor = UIColor(red: 244, green: 242, blue: 240).cgColor
        self.layer.borderWidth = 2
        self.layer.cornerRadius = 6
        self.layer.masksToBounds = true
        self.clipsToBounds = true
    }
    
}
