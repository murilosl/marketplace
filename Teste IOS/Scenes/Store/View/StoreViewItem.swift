//
//  StoreViewItem.swift
//  Teste IOS
//
//  Created by Murilo de Souza Lopes on 03/05/2019.
//  Copyright © 2019 Murilo de Souza Lopes. All rights reserved.
//

import UIKit

class StoreViewItem: UIView {

    override func draw(_ rect: CGRect) {
       self.layer.borderWidth = 2
       self.layer.borderColor = UIColor(red: 244, green: 242, blue: 240).cgColor
    }
}
