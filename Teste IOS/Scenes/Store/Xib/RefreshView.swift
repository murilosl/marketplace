//
//  RefreshView.swift
//  Teste IOS
//
//  Created by Murilo de Souza Lopes on 04/05/2019.
//  Copyright © 2019 Murilo de Souza Lopes. All rights reserved.
//

import UIKit

class RefreshView: UIView{
   
    @IBOutlet weak var image: UIImageView!

    override func draw(_ rect: CGRect) {
        image.image = UIImage.gif(name: "01-pizza-loop")
    }
}
