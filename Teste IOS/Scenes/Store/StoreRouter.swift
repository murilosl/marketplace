//
//  StoreRouter.swift
//  Teste IOS
//
//  Created by Murilo de Souza Lopes on 02/05/2019.
//  Copyright (c) 2019 Murilo de Souza Lopes. All rights reserved.
//

import UIKit

@objc protocol StoreRoutingLogic
{
    }

protocol StoreDataPassing
{
  var dataStore: StoreDataStore? { get }
  func routeToShowOrder(segue: UIStoryboardSegue?)
}

class StoreRouter: NSObject, StoreRoutingLogic, StoreDataPassing
{
  weak var viewController: StoreViewController?
  var dataStore: StoreDataStore?
  
  // MARK: Pass Data

    func passDataToProduct(source: StoreDataStore, destination: inout ProductDataStore)
    {
        
    }
    
    func routeToShowOrder(segue: UIStoryboardSegue?)
    {
        if let segue = segue {
            let destinationVC = segue.destination as! ProductViewController
            var destinationDS = destinationVC.router!.dataStore!
            passDataToProduct(source: dataStore!, destination: &destinationDS)
        }
    }

}
