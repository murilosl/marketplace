//
//  StoreViewController.swift
//  Teste IOS
//
//  Created by Murilo de Souza Lopes on 02/05/2019.
//  Copyright (c) 2019 Murilo de Souza Lopes. All rights reserved.
//


import UIKit

protocol StoreDisplayLogic: class
{
    func displaySomething(viewModel: Store.Something.ViewModel)
}

class StoreViewController: UIViewController, StoreDisplayLogic
{
    var interactor: StoreBusinessLogic?
    var router: (NSObjectProtocol & StoreRoutingLogic & StoreDataPassing)?
    var arrayProducts = [Product]()

    @IBOutlet weak var collectionView: UICollectionView!
    var refreshControl = UIRefreshControl()
    var refreshView: RefreshView!
    
    // MARK: Object lifecycle

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?)
    {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }

    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        setup()
    }

    // MARK: Setup

    private func setup()
    {
        let viewController = self
        let interactor = StoreInteractor()
        let presenter = StorePresenter()
        let router = StoreRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }

    // MARK: Routing

    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
    if let scene = segue.identifier {
        if scene=="show"{
            let productViewController = segue.destination as! ProductViewController
            let itemSelected = collectionView.indexPathsForSelectedItems?.first?.row
            productViewController.product = arrayProducts[itemSelected!]
        }
      let selector = NSSelectorFromString("routeTo\(scene)WithSegue:")
      if let router = router, router.responds(to: selector) {
        router.perform(selector, with: segue)
      }
    }
    }

    // MARK: View lifecycle

    override func viewDidLoad()
    {
        super.viewDidLoad()
        doSomething()
        collectionView.dataSource = self
        collectionView.delegate = self
        refreshSetup()
    }

    //MARK: Refresh Controll

    func refreshSetup(){
        //refreshControl.attributedTitle = NSAttributedString(string: "Carregando")
        refreshControl.backgroundColor = .clear
        refreshControl.tintColor = .clear
        refreshControl.addTarget(self, action: #selector(refresh(sender:)), for: .valueChanged)
        collectionView.refreshControl = refreshControl
    }

   
    @objc func refresh(sender:AnyObject) {
        getRefereshView()
        let request = Store.Something.Request()
        interactor?.doRefresh(request: request)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
            self.collectionView.refreshControl?.endRefreshing()
        }
    }
    
    func getRefereshView() {
        
        if let objOfRefreshView = Bundle.main.loadNibNamed("RefreshView", owner: self, options: nil)?.first as? RefreshView {
            refreshView = objOfRefreshView
            refreshView.frame = refreshControl.frame
            refreshControl.addSubview(refreshView)
        }
    }
    

    // MARK: Do something
    
    func doSomething()
    {
        let request = Store.Something.Request()
        interactor?.doSomething(request: request)
    }

    func displaySomething(viewModel: Store.Something.ViewModel)
    {
        arrayProducts = viewModel.products
        collectionView.reloadData()
        
    }
}

extension StoreViewController: UICollectionViewDataSource{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellproducts", for: indexPath) as! StoreCollectionViewCell
        
        let product = arrayProducts[indexPath.row]
        cell.populate(product: product)
        cell.configureUI()
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayProducts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        router?.routeToShowOrder(segue: nil)
    }
}

extension StoreViewController: UICollectionViewDelegate{
    
}
