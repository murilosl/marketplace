//
//  StoreWorker.swift
//  Teste IOS
//
//  Created by Murilo de Souza Lopes on 02/05/2019.
//  Copyright (c) 2019 Murilo de Souza Lopes. All rights reserved.
//


import UIKit

class StoreWorker
{
    let home_url = "https://pastebin.com/raw/qdfq10ii"
    let refresh_url = "https://pastebin.com/raw/vNWpzLB9"
    
    func doSomeWork()
    {
    }

    func fetchProducts(completion: @escaping([Product]) -> Void){
       
        NetworkManager.shared.getProducts(url: home_url, completion: { (products) in
            completion(products)
        })
    }
    
    func refreshProducts(completion: @escaping([Product]) -> Void){
        
        NetworkManager.shared.getProducts(url: refresh_url, completion: { (products) in
            completion(products)
        })
    }
}
