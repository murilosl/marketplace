//
//  StoreInteractor.swift
//  Teste IOS
//
//  Created by Murilo de Souza Lopes on 02/05/2019.
//  Copyright (c) 2019 Murilo de Souza Lopes. All rights reserved.
//


import UIKit

protocol StoreBusinessLogic
{
    func doSomething(request: Store.Something.Request)
    func doRefresh(request: Store.Something.Request)
    var product: Product? {get set }
}

protocol StoreDataStore
{
  //var name: String { get set }
    var product: Product? {get set }
}

class StoreInteractor: StoreBusinessLogic, StoreDataStore
{
    var presenter: StorePresentationLogic?
    var worker: StoreWorker?
    //var name: String = ""
    var product: Product?
    // MARK: Do something
  
    func doSomething(request: Store.Something.Request)
    {
    worker = StoreWorker()
    worker?.fetchProducts(completion: { (products) in
        let response = Store.Something.Response(products: products)
        self.presenter?.presentSomething(response: response)
    })

    }
    
    func doRefresh(request: Store.Something.Request)
    {
        worker = StoreWorker()
        worker?.refreshProducts(completion: { (products) in
            let response = Store.Something.Response(products: products)
            self.presenter?.presentSomething(response: response)
        })
        
    }
}
