//
//  StoreModels.swift
//  Teste IOS
//
//  Created by Murilo de Souza Lopes on 02/05/2019.
//  Copyright (c) 2019 Murilo de Souza Lopes. All rights reserved.
//

import UIKit

enum Store
{
  // MARK: Use cases
  
  enum Something
  {
    struct Request
    {
    }
    struct Response
    {
        let products: [Product]
    }
    struct ViewModel
    {
        let products: [Product]
    }
  }
}
