//
//  StorePresenter.swift
//  Teste IOS
//
//  Created by Murilo de Souza Lopes on 02/05/2019.
//  Copyright (c) 2019 Murilo de Souza Lopes. All rights reserved.
//


import UIKit

protocol StorePresentationLogic
{
  func presentSomething(response: Store.Something.Response)
}

class StorePresenter: StorePresentationLogic
{
  weak var viewController: StoreDisplayLogic?
  
  // MARK: Do something
  
  func presentSomething(response: Store.Something.Response)
  {
    let viewModel = Store.Something.ViewModel(products: response.products)
    viewController?.displaySomething(viewModel: viewModel)
  }
}
