//
//  Teste_IOSUITests.swift
//  Teste IOSUITests
//
//  Created by Murilo de Souza Lopes on 02/05/2019.
//  Copyright © 2019 Murilo de Souza Lopes. All rights reserved.
//
@testable import Teste_IOS

import XCTest

class Teste_IOSUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testTabHomeClick(){
        XCUIApplication().tabBars.firstMatch.buttons.element(boundBy: 0).tap()
    }
    
    func testTabSearchClick(){
        XCUIApplication().tabBars.firstMatch.buttons.element(boundBy: 1).tap()
    }
    
    func testTabPhotoClick(){
        XCUIApplication().tabBars.firstMatch.buttons.element(boundBy: 2).tap()
    }
    
    func testTabUploadClick(){
        XCUIApplication().tabBars.firstMatch.buttons.element(boundBy: 3).tap()
    }
    
    func testTabProfileClick(){
        XCUIApplication().tabBars.firstMatch.buttons.element(boundBy: 4).tap()
    }

    
    func testPullToRefresh(){
        let firstCell = XCUIApplication().staticTexts["terno delucca (blazer + calça)"]

        let start = firstCell.coordinate(withNormalizedOffset: CGVector(dx: 0, dy: 0))
        let finish = firstCell.coordinate(withNormalizedOffset: CGVector(dx: 0, dy: 30))
        start.press(forDuration: 3, thenDragTo: finish)
    }
    
    func testGoToBottom(){
        let cv = XCUIApplication().collectionViews.element(boundBy:0)
        
        let cb = cv.coordinate(withNormalizedOffset:CGVector(dx: 0, dy: 0))
        
        let scrollVector = CGVector(dx: 0.0, dy: -30.0)
        cb.press(forDuration: 0.5, thenDragTo: cb.withOffset(scrollVector))
    }
    
    func testFirstCellTap(){
        let firstCell = XCUIApplication().staticTexts["terno delucca (blazer + calça)"]
        firstCell.tap()
    }
}
